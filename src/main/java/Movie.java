import java.util.Date;

/**
 * Created by Michał Adrian Mentel on 2017-08-16.
 *
 * @nordea2
 */
public class Movie {
    private String name;
    private MovieType type;
    private Date creationDate;
    private String author;

    public Movie(String name, MovieType type, Date creationDate, String author) {
        this.name = name;
        this.type = type;
        this.creationDate = creationDate;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public MovieType getType() {
        return type;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public String getAuthor() {
        return author;
    }
}
