import java.util.HashMap;
import java.util.Map;

/**
 * Created by RENT on 2017-08-16.
 */
public class MovieDatabase {
    Map<String, Movie> movieMap = new HashMap<>();

    public void addMovie(Movie m){
        movieMap.put(m.getName(),m);
    }
    public Movie movieSearch(String movieName){
        if (movieMap.containsKey(movieName)){
            return movieMap.get(movieName);
        }
        return null;
    }

    public void listMovie(){

    }
}

